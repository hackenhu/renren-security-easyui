package io.renren.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author longzhihua
 */
@Getter
@AllArgsConstructor
public enum YesOrNoEnum {

    YES(1, "是"), NO(0, "否");
    private Integer code;
    private String desc;
}
