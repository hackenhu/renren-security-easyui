package io.renren.common.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 数字运算工具类
 * 
 * @author 胡国星
 *
 */
public class MathUtil {

	/**
	 * 精度
	 */
	public static final int precisionOf = 2;

	public static MathContext matchPrecisionOf = new MathContext(
			precisionOf + 1, RoundingMode.HALF_UP);

	/**
	 * 两数相减
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static double numberMinus(double d1, double d2) {
		return new BigDecimal(Double.toString(d1)).subtract(
				new BigDecimal(Double.toString(d2))).doubleValue();
	}

	/**
	 * 两数相加
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static double numberAdd(double d1, double d2) {
		return new BigDecimal(Double.toString(d1)).add(
				new BigDecimal(Double.toString(d2))).doubleValue();
	}

	/**
	 * 两数相除
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static double numberDivide(double d1, double d2) {
		return new BigDecimal(Double.toString(d1)).divide(
				new BigDecimal(Double.toString(d2)), precisionOf,
				RoundingMode.HALF_UP).doubleValue();
	}

	/**
	 * 两数相乘
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static double numberMultiply(double d1, double d2) {
		return new BigDecimal(Double.toString(d1)).multiply(
				new BigDecimal(Double.toString(d2)), matchPrecisionOf)
				.doubleValue();
	}
	/**
	 * 随机生成5位数集合
	 * @param n 生成个数
	 * @return Iterator<String>
	 */
   public static Iterator<String> setNum(int n){
	   Set<String> set = new HashSet<String>();
	   for (int i = 0; i < n; i++) {
	     Double random = Math.random();
	     String str = random.toString().substring(4, 8);
	     set.add(str);
	   }
	   Iterator<String> it = set.iterator();
	return it;
   }
   /**
    * 取出字符串中的数字
    * @param s 被取字符串
    * @return
    */
   public static String patString(String s){
       Pattern p = Pattern.compile("[0-9]");
       Matcher m = p.matcher(s);
       StringBuffer str = new StringBuffer();
       while (m.find()) {
           str.append(m.group());
       }
       String result = String.valueOf(str);
       return result;
   }
   /**
    * 将List字符串集合转化为字符串
    * @param str
    */
  public static String listToString(List<String> str){
      String ids = "";
      for (String string : str) {
          if("".equals(ids)){
              ids = string;
          }else{
              ids = ids+","+string;
          }
      }
      return ids;
  }
  
	/**
	 * 数据格式化，将double类型数据以四舍五入的方式保留n位小数
	 * @Title: numberFormat 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param number 数据
	 * @param precisionOf 保留小数位
	 * @return
	 * @Date 2017年3月30日 上午10:02:48
	 */
	
	public static String numberFormat(double number, int precisionOf) {
      BigDecimal bigDecimal = new BigDecimal(number);  
      return bigDecimal.setScale(precisionOf, BigDecimal.ROUND_HALF_UP).toString();  
	}
	
//   public static void main(String[] args) {
//       List<String> str = new ArrayList<String>();
//       str.add("22");
//       str.add("22");
//       str.add("22");
//       System.out.println(listToString(str));
//   }
}
