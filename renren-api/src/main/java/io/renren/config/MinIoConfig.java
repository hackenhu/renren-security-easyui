package io.renren.config;

import io.minio.MinioClient;
import io.renren.common.exception.RRException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinIoConfig {

    @Value("${minio.endpoint}")
    protected String endpoint;

    @Value("${minio.bucketName}")
    protected String bucketName;

    @Value("${minio.accessKey}")
    protected String accessKey;

    @Value("${minio.secretKey}")
    protected String secretKey;

    @Bean
    public MinioClient minioClient(){
        MinioClient client = null;
        try {
            client = new MinioClient(endpoint, accessKey, secretKey);
        } catch (Exception e) {
            new RRException("MinIO对象存储注入失败");
        }
        return client;
    }
}
