package io.renren.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.renren.annotation.Login;
import io.renren.common.validator.ValidatorUtils;
import io.renren.entity.CountryEntity;
import io.renren.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import io.renren.common.utils.PageInfo;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
@RestController
@RequestMapping("parking/country")
public class CountryController {
    @Autowired
    private CountryService countryService;

    /**
     * 列表
     */
    @Login
    @RequestMapping("/list")
    public PageInfo list(@RequestParam Map<String, Object> params){
        IPage<CountryEntity> page =  countryService.queryPage(params);
        PageInfo pageInfo = new PageInfo();
        pageInfo.setTotal((int) page.getTotal());
        pageInfo.setRows(page.getRecords());
        return pageInfo;
    }


    /**
     * 信息
     */
    @Login
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
        CountryEntity country = countryService.getById(id);

        return R.ok().put("obj", country);
    }

    /**
     * 保存
     */
    @Login
    @RequestMapping("/save")
    public R save(@RequestBody CountryEntity country){
        countryService.save(country);

        return R.ok();
    }

    /**
     * 修改
     */
    @Login
    @RequestMapping("/update")
    public R update(@RequestBody CountryEntity country){
        ValidatorUtils.validateEntity(country);
        countryService.updateById(country);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @Login
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
        countryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    @Login
    @RequestMapping("/getComboboxData")
    public List<Map<String, Object>> getComboboxData(@RequestParam Map<String, Object> params) {
        List<CountryEntity> list = countryService.list(new QueryWrapper<CountryEntity>().select("id,name"));
        List<Map<String,Object>> maplist = Lists.newArrayList();
        if(!list.isEmpty()){
            for(CountryEntity entity:list){
                Map<String,Object> map = Maps.newHashMap();
                map.put("id",entity.getId());
                map.put("text",entity.getName());
                maplist.add(map);
            }
        }
        return maplist;
    }

}
