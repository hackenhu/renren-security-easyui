package io.renren.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.annotation.Login;
import io.renren.common.validator.ValidatorUtils;
import io.renren.entity.StreetEntity;
import io.renren.service.StreetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import io.renren.common.utils.PageInfo;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
@RestController
@RequestMapping("parking/street")
public class StreetController {
    @Autowired
    private StreetService streetService;

    /**
     * 列表
     */
    @Login
    @RequestMapping("/list")
    public PageInfo list(@RequestParam Map<String, Object> params){
        IPage<StreetEntity> page =  streetService.queryPage(params);
        PageInfo pageInfo = new PageInfo();
        pageInfo.setTotal((int) page.getTotal());
        pageInfo.setRows(page.getRecords());
        return pageInfo;
    }


    /**
     * 信息
     */
    @Login
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
        StreetEntity street = streetService.getById(id);

        return R.ok().put("obj", street);
    }

    /**
     * 保存
     */
    @Login
    @RequestMapping("/save")
    public R save(@RequestBody StreetEntity street){
        streetService.save(street);

        return R.ok();
    }

    /**
     * 修改
     */
    @Login
    @RequestMapping("/update")
    public R update(@RequestBody StreetEntity street){
        ValidatorUtils.validateEntity(street);
        streetService.updateById(street);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @Login
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
        streetService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
