package io.renren.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.annotation.Login;
import io.renren.common.validator.ValidatorUtils;
import io.renren.entity.CommunityEntity;
import io.renren.service.CommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import io.renren.common.utils.PageInfo;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
@RestController
@RequestMapping("parking/community")
public class CommunityController {
    @Autowired
    private CommunityService communityService;

    /**
     * 列表
     */
    @Login
    @RequestMapping("/list")
    public PageInfo list(@RequestParam Map<String, Object> params){
        IPage<CommunityEntity> page =  communityService.queryPage(params);
        PageInfo pageInfo = new PageInfo();
        pageInfo.setTotal((int) page.getTotal());
        pageInfo.setRows(page.getRecords());
        return pageInfo;
    }


    /**
     * 信息
     */
    @Login
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        CommunityEntity community = communityService.getById(id);

        return R.ok().put("obj", community);
    }

    /**
     * 保存
     */
    @Login
    @RequestMapping("/save")
    public R save(@RequestBody CommunityEntity community){
        communityService.save(community);

        return R.ok();
    }

    /**
     * 修改
     */
    @Login
    @RequestMapping("/update")
    public R update(@RequestBody CommunityEntity community){
        ValidatorUtils.validateEntity(community);
        communityService.updateById(community);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @Login
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
        communityService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
