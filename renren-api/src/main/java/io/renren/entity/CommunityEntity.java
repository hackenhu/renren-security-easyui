package io.renren.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
@Data
@TableName("community")
public class CommunityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private Integer streetId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 城乡分类代码
	 */
	private String category;

}
