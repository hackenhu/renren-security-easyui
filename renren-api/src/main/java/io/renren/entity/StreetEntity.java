package io.renren.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
@Data
@TableName("street")
public class StreetEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private Integer countryId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String href;

}
