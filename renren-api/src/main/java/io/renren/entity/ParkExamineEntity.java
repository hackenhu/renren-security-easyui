package io.renren.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 停车场调查表
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-27 21:25:27
 */
@Data
@TableName("tb_park_examine")
public class ParkExamineEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long id;
	/**
	 * 停车场名称
	 */
	private String parkName;
	/**
	 * 市
	 */
	private String cityName;
	/**
	 * 市
	 */
	private Integer cityId;
	/**
	 * 区
	 */
	private String areaName;
	private Integer areaId;
	/**
	 * 街道
	 */
	private String streetName;
	private Integer streetId;
	/**
	 * 社区
	 */
	private String communityName;
	private Integer communityId;
	/**
	 * 路
	 */
	private String roadName;
	/**
	 * 号
	 */
	private Integer number;
	/**
	 * 经营单位名称
	 */
	private String businessUnit;
	/**
	 * 产权单位名称
	 */
	private String propertyUnit;
	/**
	 * 联系人
	 */
	private String contacts;
	/**
	 * 联系电话
	 */
	private String contactsPhone;
	/**
	 * 收费方式
	 */
	private String chargingMethod;
	/**
	 * 停车场类别
	 */
	private String parkType;
	/**
	 * 入口数量
	 */
	private Integer entranceNum;
	/**
	 * 入口经度，多个
	 */
	private String entranceLongitude;
	/**
	 * 入口纬度，多个
	 */
	private String entranceLatitude;
	/**
	 * 停车位总数
	 */
	private Integer placeNum;
	/**
	 * 地上数量
	 */
	private Integer roadUpNum;
	/**
	 * 地下数量
	 */
	private Integer roadDownNum;
	/**
	 * 出售数量
	 */
	private Integer sellNum;
	/**
	 * 挪用数量
	 */
	private Integer divertNum;
	/**
	 * 改建数量
	 */
	private Integer rebuildNum;
	/**
	 * 充电桩数量
	 */
	private Integer chargingPileNum;
	/**
	 * 共享数量
	 */
	private Integer shareNum;
	/**
	 * 道闸品牌
	 */
	private String barrierType;
	/**
	 * 是否有网络
	 */
	private Integer isNetwork;
	/**
	 * 街道审核状态 0 审核中 1 审核通过 2 驳回
	 */
	private Integer streetReviewStatus;
	/**
	 * 街道审核人id
	 */
	private Long streetReviewUserId;
	/**
	 * 街道审核人
	 */
	private String streetReviewUser;
	/**
	 * 区审核状态 0 审核中 1 审核通过 2 驳回
	 */
	private Integer areaReviewStatus;
	/**
	 * 区审核人id
	 */
	private Long areaReviewUserId;
	/**
	 * 区审核人
	 */
	private String areaReviewUser;
	/**
	 * 市审核状态 0 审核中 1 审核通过 2 驳回
	 */
	private Integer cityReviewStatus;
	/**
	 * 区审核人id
	 */
	private Long cityReviewUserId;
	/**
	 * 区审核人
	 */
	private String cityReviewUser;
	/**
	 * 创建人id
	 */
	private Long createUserId;
	/**
	 * 创建人
	 */
	private String createUser;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	/**
	 * 部门id
	 */
	private Long deptId;
	/**
	 * 采集类型 0 居住类 1 非居住类 2 公共停车场
	 */
	private Integer type;
	/**
	 * 上午10点饱和度
	 */
	private Long tenSaturation;
	/**
	 * 晚上11点饱和度
	 */
	private Long twentyThreeSaturation;
	/**
	 * 停车场照片
	 */
	private String parkUrl;
	/**
	 * 审核状态 -1 未审核 0 审核中 1 审核通过 2 驳回
	 */
	private Integer reviewStatus;
}
