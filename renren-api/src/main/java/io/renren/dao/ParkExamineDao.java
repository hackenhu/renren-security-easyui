package io.renren.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.entity.ParkExamineEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 停车场调查表
 * 
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-27 21:25:27
 */
@Mapper
public interface ParkExamineDao extends BaseMapper<ParkExamineEntity> {
	
}
