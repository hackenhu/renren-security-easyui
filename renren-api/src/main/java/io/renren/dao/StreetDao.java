package io.renren.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.entity.StreetEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
@Mapper
public interface StreetDao extends BaseMapper<StreetEntity> {
	
}
