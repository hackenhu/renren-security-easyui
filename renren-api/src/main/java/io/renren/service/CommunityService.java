package io.renren.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.entity.CommunityEntity;

import java.util.Map;

/**
 * 
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
public interface CommunityService extends IService<CommunityEntity> {

    IPage queryPage(Map<String, Object> params);
}

