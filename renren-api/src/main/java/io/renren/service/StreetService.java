package io.renren.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.entity.StreetEntity;

import java.util.Map;

/**
 * 
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-28 11:18:23
 */
public interface StreetService extends IService<StreetEntity> {

    IPage queryPage(Map<String, Object> params);
}

