package io.renren.service.impl;

import io.renren.dao.StreetDao;
import io.renren.entity.StreetEntity;
import io.renren.form.EasyUIQuery;
import io.renren.service.StreetService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;



@Service("streetService")
public class StreetServiceImpl extends ServiceImpl<StreetDao, StreetEntity> implements StreetService {

    @Override
    public IPage queryPage(Map<String, Object> params) {
        StreetEntity entity = new StreetEntity();
        if(params.get("countryId") != null){
            entity.setCountryId(Integer.valueOf(params.get("countryId").toString()));
        }
        IPage<StreetEntity> page = this.page(
                new EasyUIQuery<StreetEntity>().getPage(params),
                new QueryWrapper<StreetEntity>(entity));
        return page;
    }

}
