package io.renren.service.impl;

import io.renren.dao.CountryDao;
import io.renren.entity.CountryEntity;
import io.renren.form.EasyUIQuery;
import io.renren.service.CountryService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;



@Service("countryService")
public class CountryServiceImpl extends ServiceImpl<CountryDao, CountryEntity> implements CountryService {

    @Override
    public IPage queryPage(Map<String, Object> params) {
        CountryEntity entity = new CountryEntity();
        IPage<CountryEntity> page = this.page(
                new EasyUIQuery<CountryEntity>().getPage(params),
                new QueryWrapper<CountryEntity>(entity));
        return page;
    }

}
