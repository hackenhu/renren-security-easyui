package io.renren.service.impl;

import io.renren.dao.CommunityDao;
import io.renren.entity.CommunityEntity;
import io.renren.form.EasyUIQuery;
import io.renren.service.CommunityService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;



@Service("communityService")
public class CommunityServiceImpl extends ServiceImpl<CommunityDao, CommunityEntity> implements CommunityService {

    @Override
    public IPage queryPage(Map<String, Object> params) {
        CommunityEntity entity = new CommunityEntity();
        if(params.get("streetId") != null){
            entity.setStreetId(Integer.valueOf(params.get("streetId").toString()));
        }
        IPage<CommunityEntity> page = this.page(
                new EasyUIQuery<CommunityEntity>().getPage(params),
                new QueryWrapper<CommunityEntity>(entity));
        return page;
    }

}
