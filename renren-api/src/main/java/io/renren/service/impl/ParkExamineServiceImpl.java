package io.renren.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.dao.ParkExamineDao;
import io.renren.entity.ParkExamineEntity;
import io.renren.entity.SysUserEntity;
import io.renren.form.EasyUIQuery;
import io.renren.service.ParkExamineService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("parkExamineService")
public class ParkExamineServiceImpl extends ServiceImpl<ParkExamineDao, ParkExamineEntity> implements ParkExamineService {

    @Override
    public IPage queryPage(Map<String, Object> params, SysUserEntity user) {
        ParkExamineEntity entity = new ParkExamineEntity();
        entity.setCreateUserId(user.getUserId());
        IPage<ParkExamineEntity> page = this.page(
                new EasyUIQuery<ParkExamineEntity>().getPage(params),
                new QueryWrapper<ParkExamineEntity>(entity));
        return page;
    }

}
