package io.renren.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.annotation.LoginUser;
import io.renren.entity.ParkExamineEntity;
import io.renren.entity.SysUserEntity;

import java.util.Map;

/**
 * 停车场调查表
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-01-27 21:25:27
 */
public interface ParkExamineService extends IService<ParkExamineEntity> {

    IPage queryPage(Map<String, Object> params, SysUserEntity user);
}

