var mainDataGrid;
$(function () {
    initDataGrid(null);
});

function initDataGrid(queryParams){
    mainDataGrid = $('#mainDataGrid').datagrid({
        url : applicationPath+'/sys/student/list',
        method:'GET',
        queryParams : queryParams,
        striped : true,
        idField : 'id',
        rownumbers : true,
        pagination : true,
        singleSelect : true,
        fitColumns: false,
        sortOrder : 'asc',
        pageSize : 20,
        pageList : [ 10, 20, 30, 40, 50, 100, 200, 300, 400, 500 ],
        columns : [ [                                        {
                    field : 'name',
                    title : '姓名',
                    width : 100
                },                                     {
                    field : 'sex',
                    title : '性别',
                    width : 100
                },                                     {
                    field : 'studentNo',
                    title : '学号',
                    width : 100
                },                                     {
                    field : 'school',
                    title : '学校',
                    width : 100
                },                                     {
                    field : 'teacher',
                    title : '老师',
                    width : 100
                },                                     {
                    field : 'major',
                    title : '专业',
                    width : 100
                }                           ] ]
    });
}

function searchForm(){
    initDataGrid($("#searchForm").serializeObject());
}

function addFun(){
    parent.layer.open({
        type: 2,
        area: ['580px', '70%'],
        fixed: false, //不固定
        maxmin: true,
        title:"添加",
        btn: ['确定',"取消"],
        content: applicationPath+'/modules/sys/studentedit.html',
        yes: function(index, layero){
            var formdata = layero.find("iframe")[0].contentWindow.$("#editForm").serializeObject();
            submit(formdata,applicationPath+"/sys/student/save");
            parent.layer.close(index); //如果设定了yes回调，需进行手工关闭
        }
    });
}

function editFun(){
    var node = mainDataGrid.datagrid('getSelected');
    if (node) {
        parent.layer.open({
            type: 2,
            area: ['580px', '70%'],
            fixed: false, //不固定
            maxmin: true,
            title:"编辑",
            btn: ['确定',"取消"],
            content: applicationPath+'/modules/sys/studentedit.html?id='+node.id,
            yes: function(index, layero){
                var formdata = layero.find("iframe")[0].contentWindow.$("#editForm").serializeObject();
                submit(formdata,applicationPath+"/sys/student/update");
                parent.layer.close(index); //如果设定了yes回调，需进行手工关闭
            }
        });
    }else{
        parent.layer.msg("请先选中行",{icon: 2});
    }
}

function deleteFun() {
    var node = mainDataGrid.datagrid('getSelected');
    if (node) {
        var ids = [node.id];
        parent.layer.confirm('您是否要删除当前选中行!', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url : applicationPath+"/sys/student/delete",
                type : "POST",
                dataType: "json",
                contentType: "application/json;charset=UTF-8",
                data: JSON.stringify(ids),
                success : function(result) {
                    if (result.code == 0) {
                        parent.layer.msg('操作成功', {icon: 1});
                        mainDataGrid.datagrid('reload');
                        parent.layer.close(index); //如果设定了yes回调，需进行手工关闭
                    } else {
                        parent.layer.alert(result.msg,{icon: 2});
                    }
                },
                error:function(msg){
                    parent.layer.alert(msg);
                }
            });
        }, function(){

        });
    }else{
        parent.layer.msg("请先选中行",{icon: 2});
    }
}

function submit(data,url){
    $.ajax({
        url : url,
        type : "POST",
        dataType: "json",
        contentType: "application/json;charset=UTF-8",
        data : JSON.stringify(data),
        success : function(result) {
            if (result.code == 0) {
                parent.layer.msg("编辑成功",{icon: 1});
                mainDataGrid.datagrid('reload');
            } else {
                parent.layer.alert(result.msg,{icon: 2});
            }
        },
        error:function(msg){
            layer.alert(msg);
        }
    });
}