package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 学生信息
 * 
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-11-24 14:32:22
 */
@Data
@TableName("tb_student")
public class StudentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 学号
	 */
	private String studentNo;
	/**
	 * 学校
	 */
	private String school;
	/**
	 * 老师
	 */
	private String teacher;
	/**
	 * 专业
	 */
	private String major;

}
