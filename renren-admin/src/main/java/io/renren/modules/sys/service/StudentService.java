package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.renren.modules.sys.entity.StudentEntity;

import java.util.Map;

/**
 * 学生信息
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-11-24 14:32:22
 */
public interface StudentService extends IService<StudentEntity> {

    IPage queryPage(Map<String, Object> params);
}

