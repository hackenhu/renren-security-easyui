package io.renren.modules.sys.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.renren.modules.sys.dao.StudentDao;
import io.renren.modules.sys.entity.StudentEntity;
import io.renren.modules.sys.service.StudentService;

import io.renren.common.utils.EasyUIQuery;


@Service("studentService")
public class StudentServiceImpl extends ServiceImpl<StudentDao, StudentEntity> implements StudentService {

    @Override
    public IPage queryPage(Map<String, Object> params) {
        StudentEntity entity = new StudentEntity();
        IPage<StudentEntity> page = this.page(
                new EasyUIQuery<StudentEntity>().getPage(params),
                new QueryWrapper<StudentEntity>(entity));
        return page;
    }

}
