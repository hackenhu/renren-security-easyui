package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import io.renren.modules.sys.entity.StudentEntity;
import io.renren.modules.sys.service.StudentService;
import io.renren.common.utils.PageInfo;
import io.renren.common.utils.R;



/**
 * 学生信息
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-11-24 14:32:22
 */
@RestController
@RequestMapping("sys/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:student:list")
    public PageInfo list(@RequestParam Map<String, Object> params){
        IPage<StudentEntity> page =  studentService.queryPage(params);
        PageInfo pageInfo = new PageInfo();
        pageInfo.setTotal((int) page.getTotal());
        pageInfo.setRows(page.getRecords());
        return pageInfo;
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:student:info")
    public R info(@PathVariable("id") Long id){
        StudentEntity student = studentService.getById(id);

        return R.ok().put("obj", student);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:student:save")
    public R save(@RequestBody StudentEntity student){
        studentService.save(student);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:student:update")
    public R update(@RequestBody StudentEntity student){
        ValidatorUtils.validateEntity(student);
        studentService.updateById(student);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:student:delete")
    public R delete(@RequestBody Long[] ids){
        studentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
