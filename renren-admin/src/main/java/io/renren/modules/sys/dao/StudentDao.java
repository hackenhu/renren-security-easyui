package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.StudentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 学生信息
 * 
 * @author hacken
 * @email hacken_hu@foxmail.com
 * @date 2021-11-24 14:32:22
 */
@Mapper
public interface StudentDao extends BaseMapper<StudentEntity> {
	
}
