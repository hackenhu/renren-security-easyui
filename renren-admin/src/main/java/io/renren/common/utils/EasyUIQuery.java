/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.xss.SQLFilter;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * 查询参数
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 */
public class EasyUIQuery<T> {

	public IPage<T> getPage(Map<String, Object> params) {
		return this.getPage(params, null, false);
	}

	public IPage<T> getPage(Map<String, Object> params, String defaultOrderField, boolean isAsc) {
		// 分页参数
		long curPage = 1;
		long rows = 10;

		if (params.get(Constant.PAGE) != null) {
			curPage = Long.parseLong((String) params.get(Constant.PAGE));
		}
		if (params.get(Constant.ROWS) != null) {
			rows = Long.parseLong((String) params.get(Constant.ROWS));
		}

		// 分页对象
		Page<T> page = new Page<>(curPage, rows);

		// 分页参数
		params.put(Constant.PAGE, page);

		// 排序字段
		// 防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）
		String orderField = (String) params.get(Constant.SORT);
		String order = (String) params.get(Constant.ORDER);

		// 前端字段排序
		if (StringUtils.isNotEmpty(orderField) && StringUtils.isNotEmpty(order)) {
			orderField = SQLFilter.sqlInject(HumpToUnderline(orderField));
			if (Constant.ASC.equalsIgnoreCase(order)) {
				return page.addOrder(OrderItem.asc(orderField));
			} else {
				return page.addOrder(OrderItem.desc(orderField));
			}
		}

		// 没有排序字段，则不排序
		if (StringUtils.isBlank(defaultOrderField)) {
			return page;
		}

		// 默认排序
		if (isAsc) {
			page.addOrder(OrderItem.asc(defaultOrderField));
		} else {
			page.addOrder(OrderItem.desc(defaultOrderField));
		}

		return page;
	}

	/***
	 * 驼峰命名转为下划线命名
	 *
	 * @param para 驼峰命名的字符串
	 */
	public static String HumpToUnderline(String para) {
		StringBuilder sb = new StringBuilder(para);
		int temp = 0;// 定位
		if (!para.contains("_")) {
			for (int i = 0; i < para.length(); i++) {
				if (Character.isUpperCase(para.charAt(i))) {
					sb.insert(i + temp, "_");
					temp += 1;
				}
			}
		}
		return sb.toString();
	}
}
