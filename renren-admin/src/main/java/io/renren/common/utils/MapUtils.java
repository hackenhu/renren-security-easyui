package io.renren.common.utils;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * Map 工具类
 *
 * @author hacken
 * @email hacken_hu@foxmail.com
 */
public class MapUtils {

    /**
     * 分页参数转换
     *
     * @param params
     * @return
     */
    public static Map<String, Object> getPageParams(Map<String, Object> params) {
        // 分页参数
        int page = 1;
        int rows = 20;

        if (params.get(Constant.PAGE) != null) {
            page = Integer.parseInt(params.get(Constant.PAGE).toString());
        }
        if (params.get(Constant.ROWS) != null) {
            rows = Integer.parseInt(params.get(Constant.ROWS).toString());
        }
        page = (page - 1) * rows;
        params.remove("page");
        params.put("page", page);
        return params;
    }

    public static void main(String[] args) {
        MapUtils util = new MapUtils();
        Map<String, Object> map = Maps.newHashMap();
        map.put("page", 1);
        map.put("rows", 20);
        System.out.println(util.getPageParams(map));
    }
}
